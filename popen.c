#define MAX_NUM_CHARS 2000
#include <stdio.h>
#include <stdlib.h>

int 
main(int argc, char *argv[])
{
    FILE *f;
    int i;
    char line[MAX_NUM_CHARS];

    f = popen("tar jxOf foo.tar.bz2  a", "r");
    if(!f) {
	fprintf(stderr, "problem creating the pipe");
	exit(2);
    }

    while(fgets(line, MAX_NUM_CHARS, f)) {
	fprintf(stderr, "%d %s", i++, line);
    }

    return(0);
}
